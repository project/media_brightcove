
Media: Brightcove

Initial Development by Aaron Winborn

This adds support for the custom premium players of Brightcove, available at
http://brightcove.com/.

To use this module, you'll first need to install the Media module, which is
available at http://drupal.org/project/media.

Make sure to visit /admin/config/media/brightcove to enter your provider id.
