<?php

/**
 *  @file
 *  Create a Brightcove Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $brightcove = new ResourceBrightcoveStreamWrapper('brightcove://pub/[publisher-code]/id/[video-code]');
 */
class MediaBrightcoveStreamWrapper extends MediaReadOnlyStreamWrapper {
  static function getMimeType($uri, $mapping = NULL) {
    return 'video/brightcove';
  }

  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    $code = $parts['id'];
    $data = media_brightcove_data($code);
    if (!empty($data['videoStillURL'])) {
      return $data['videoStillURL'];
    }
    if (!empty($data['thumbnailURL'])) {
      return $data['thumbnailURL'];
    }
    return FALSE;
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $code = $parts['id'];
    $local_path = "public://media-brightcove/$code.jpg";
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $original_path = $this->getOriginalThumbnailPath();
      if ($original_path) {
        @copy($original_path, $local_path);
      }
      else {
        $icon_dir = media_variable_get('icon_base_directory') . '/' . media_variable_get('icon_set');
        $mimetype = $this->getMimeType(NULL);
        $local_path = "public://media-brightcove/generic.jpg";
        copy(file_icon_path((object)array('filemime' => $mimetype), $icon_dir), $local_path);
      }
    }
    return $local_path;
  }
}
