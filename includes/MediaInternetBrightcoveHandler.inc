<?php

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetBrightcoveHandler extends MediaInternetBaseHandler {
  /**
   * Parse an embed code to see if it's a Brightcove URL.
   */
  public function parse($embedCode) {
    $url = parse_url($embedCode);
    if ($url === FALSE) {
      return;
    }
    // Handle URLs in the form
    // http://link.brightcove.com/services/player/bcpid1028015867001?bckey=AQ~~,AAAA70l4HAE~,gF9XnpoghZfah2RMClt3q3hoINHflb-S&bctid=1028726494001
    if ($url['host'] == 'link.brightcove.com') {
      $query = array();
      parse_str($url['query'], $query);
      if (isset($query['bctid'])) {
        $videoCode = $query['bctid'];
      }
    }
    // Handle URLs in the form http://bcove.me/2utojvl8
    elseif ($url['host'] == 'bcove.me') {
      // This is a shortened URL. There doesn't seem to be a way to get the ID of
      // a video from the shortened URL without doing this:
      $fetch = drupal_http_request($parse, array(), 'GET', NULL, 0);
      preg_match('/bctid=(\d+)/', $fetch->headers['Location'], $matches);
      $videoCode = $matches[1];
    }
    if (!empty($videoCode)) {
      return file_stream_wrapper_uri_normalize(media_brightcove_uri($videoCode));
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function validate() {
    // Make sure users don't upload the same video twice.
    // @todo Media module should handle this: http://drupal.org/node/952422
    $uri = $this->parse($this->embedCode);
    $existing_files = file_load_multiple(array(), array('uri' => $uri));
    if (count($existing_files)) {
      throw new MediaInternetValidationException(t('You have entered a URL for a video that is already in your library.'));
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    return file_uri_to_object($uri);
  }
}
