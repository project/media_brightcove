<?php

/**
 * @file
 * File formatters for Brightcove videos.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_brightcove_file_formatter_info() {
  $formatters['media_brightcove_video'] = array(
    'label' => t('Brightcove Video'),
    'file types' => array('video'),
    'default settings' => array(
      'width' => 486,
      'height' => 412,
    ),
    'view callback' => 'media_brightcove_file_formatter_video_view',
    'settings callback' => 'media_brightcove_file_formatter_video_settings',
    'mime types' => array('video/brightcove'),
  );

  $formatters['media_brightcove_image'] = array(
    'label' => t('Brightcove Preview Image'),
    'file types' => array('video'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_brightcove_file_formatter_image_view',
    'settings callback' => 'media_brightcove_file_formatter_image_settings',
    'mime types' => array('video/brightcove'),
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_brightcove_file_formatter_video_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'brightcove') {
    $variables = array('video_id' => media_brightcove_extract_id_from_uri($file->uri));

    foreach (array('width', 'height') as $setting) {
      $variables[$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }

    $element = array(
      '#markup' => theme('media_brightcove_video', $variables),
    );

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_brightcove_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();

  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
  );

  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
  );

  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_brightcove_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'brightcove') {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);

    if (empty($image_style) || !isset($valid_image_styles[$image_style])) {
      $element = array(
        '#theme' => 'image',
        '#path' => $wrapper->getOriginalThumbnailPath(),
      );
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
      );
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_brightcove_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();

  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );

  return $element;
}
