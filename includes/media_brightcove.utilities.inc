<?php

/**
 * @file includes/media_brightcove.utilities.includes
 *
 * Utility functions for Media: Brightcove.
 */

/**
 * Read and store the raw data from Brightcove.
 *
 * @param string $code
 *  The URI or Brightcove ID.
 * @return array
 *  The array of the data as returned from Brightcove's API.
 */
function _media_brightcove_raw_data($code) {
  $code = media_brightcove_extract_id_from_uri($code);

  // Initialize the data array.
  $data = array();

  if ($token = variable_get('media_brightcove_read_token', '')) {
    // Connect and fetch a value.
    $url = url('http://api.brightcove.com/services/library', array('query' => array('command' => 'find_video_by_id', 'video_id' => $code, 'token' => $token), 'external' => TRUE));
    $result = drupal_http_request($url);

    if ($result->code != 200) {
      $error_text = 'Brightcove: HTTP error @info when fetching video data from URL !url.';
      $error_vars = array('@info' => "$result->code: $result->error", '!url' => $url);
      watchdog('media_brightcove', $error_text, $error_vars, WATCHDOG_WARNING);
    }
    else {
      $data = json_decode($result->data, TRUE);
      if (!$data) {
        watchdog('media_brightcove', 'Brightcove: Error parsing JSON from !url', array('!url' => $url), WATCHDOG_WARNING);
        // Reset data.
        $data = array();
      }
    }
  }
  else {
    watchdog('media_brightcove', "No !token available for Brightcove's API.", array('!token' => l(t('read token'), 'admin/settings/media_brightcove')), WATCHDOG_WARNING);
    // TODO: If we have no API token, grab alternative data.
    // @see http://forum.brightcove.com/t5/General-Dev-Questions/Discover-video-still-thumbnail-without-API/td-p/6353
    // $url = url('http://link.brightcove.com/services/link/bcpid'. $code, array('query' => array('action' => 'mrss', 'v' => '2'), 'external' => TRUE));
    // $data['media_brightcove_rss'] = media_retrieve_xml($url);
  }


  // Create some version control. Thus if we make changes to the data array
  // down the road, we can respect older content. If allowed by Embedded Media
  // Field, any older content will automatically update this array as needed.
  // In any case, you should account for the version if you increment it.
//     $data['emvideo_brightcove_version'] = MEDIA_BRIGHTCOVE_DATA_VERSION;

  // Set our defaults.
  global $user;
  $uri = media_brightcove_uri($code);
  $item = array(
    'uri' => $uri,
    'uid' => $user->uid,
    'status' => 1,
    'timestamp' => time(),
    'data' => serialize($data),
    'title' => isset($data['name']) ? $data['name'] : '',
    'duration' => isset($data['length']) ? round($data['length'] / 1000) : 0,
    'description' => isset($data['shortDescription']) ? $data['shortDescription'] : 0,
    'thumbnail' => $data['videoStillURL'] ? $data['videoStillURL'] : (isset($data['thumbnailURL']) ? $data['thumbnailURL'] : ''),
  );

  // Merge any stored results, overwriting the defaults.
  $results = db_query("SELECT uid, status, timestamp FROM {media_brightcove_metadata} WHERE uri = '%s'", array('uri' => $uri));
  foreach ($results as $result) {
    $item = array_merge($item, $result);
  }

  // We need to see if the record actually exists for the db write.
  // TODO: Use db_merge() instead.
  $exists = db_query("SELECT uri FROM {media_brightcove_metadata} WHERE uri = :uri", array(':uri' => $uri))->fetchField();
  drupal_write_record('media_brightcove_metadata', $item, $exists ? array('uri') : array());

  return $data;
}
