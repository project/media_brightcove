
/**
 * @file
 * Create a player preview for the Media: Brightcove administration screen.
 */
(function ($) {
  /**
   * Return HTML to preview the desired player from Brightcove.
   *
   * @param string playerID
   *  The Brightcove player ID.
   * @return string
   *  The HTML to preview the player.
   */
  mediaBrightcovePlayerHTML = function (playerID) {
    var publisherID = $('#edit-media-brightcove-publisher-id').attr('value');
    var width = Drupal.settings.mediaBrightcove.adminSettings.width;
    var height = Drupal.settings.mediaBrightcove.adminSettings.height;
    var videoID = '';
    var output = '';

    // We'll only build the HTML if we were given a player ID.
    if (playerID) {
      output = '<object width="' + width + '" height="' + height + '" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,47,0"><param name="movie" value="http://c.brightcove.com/services/viewer/federated_f9/' + playerID + '?isVid=1&isUI=1&publisherID=' + publisherID + '" /><param name="bgcolor" value="#FFFFFF" /><param name="flashVars" value="videoId=' + videoID + '&playerID=' + playerID + '&domain=embed&" /><param name="base" value="http://admin.brightcove.com" /><param name="seamlesstabbing" value="false" /><param name="allowFullScreen" value="true" /><param name="swLiveConnect" value="true" /><param name="allowScriptAccess" value="always" /><embed src="http://c.brightcove.com/services/viewer/federated_f9/' + playerID + '?isVid=1&isUI=1&publisherID=' + publisherID + '" bgcolor="#FFFFFF" flashVars="videoId=' + videoID + '&playerID=' + playerID + '&domain=embed&" base="http://admin.brightcove.com" name="flashObj" width="' + width + '" height="' + height + '" seamlesstabbing="false" type="application/x-shockwave-flash" allowFullScreen="true" swLiveConnect="true" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"></embed></object>';
    }

    return output;
  }

  /**
   * Behaviors for the Media: Brightcove administration screen.
   */
  Drupal.behaviors.mediaBrightcovePreviewPlayer = {};
  Drupal.behaviors.mediaBrightcovePreviewPlayer.attach = function (context) {
    // Display the initial full size player preview.
    $('#media-brightcove-player-id-preview').once('mediaBrightcovePreviewPlayer').html(mediaBrightcovePlayerHTML($('#edit-media-brightcove-player-id').attr('value')));

    // When this textfield is changed, update the preview accordingly.
    $('#edit-media-brightcove-player-id').once('mediaBrightcovePreviewPlayer').change(function () {
      $('#media-brightcove-player-id-preview').html(mediaBrightcovePlayerHTML($('#edit-media-brightcove-player-id').attr('value')));
    });

  }
})(jQuery);
