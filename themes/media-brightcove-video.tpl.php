<?php

/**
 * @file
 * Theme template for Media: Brightcove files.
 *
 * Available variables:
 *  $div_id         // A unique CSS id.
 *  $classes        // The CSS class string.
 *  $width          // The width of the video.
 *  $height         // The height of the video.
 *  $player_id      // The Brightcove player ID.
 *  $publisher_id   // The publisher ID.
 *  $video_id       // The Brightcove video ID.
 *  $full_size      // Whether to display the video as full size or preview.
 *  $output         // The full output to display.
 *  $availability   // Whether the video is available or not.
 *  $item           // The original field item.
 *  $autoplay       // Whether to autoplay or not.
 *  $options        // Various option overrides passed to the theme.
 */
?>
<div id="<?php print $div_id; ?>" class="<?php print $classes; ?>"><?php print $output; ?></div>