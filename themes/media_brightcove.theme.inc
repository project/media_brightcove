<?php

/**
 * @file
 * Theme functions for Media: Brightcove.
 */

function theme_media_brightcove_unavailable_message($variables) {
  $message = filter_xss($variables['message']);
  return "<div class='media-youtube-status-unavailable'>$message</div>";
}

function template_preprocess_media_brightcove_videos(&$variables) {
  static $count = 0;

  $variables['videos'] = array();
  foreach($variables['video_ids'] as $id) {
    $variables['videos'][] = theme('media_brightcove_video', array('video_id' => $id, 'width' => $variables['width'], 'height' => $variables['height']));
  }
  $variables['classes'] = array('media-brightcove-playlist');
  $variables['output'] = theme('item_list', array('videos' => $variables['videos']));
  $variables['div_id'] = 'media-brightcove-'. ($count++);
}

/**
 * The embedded flash displaying the brightcove video.
 */
function template_preprocess_media_brightcove_video(&$variables) {
  static $count = 0;

  if (!empty($variables['video_id'])) {
    $publisher_id = !empty($variables['publisher_id']) ? $variables['publisher_id'] : (isset($variables['options']['field']['widget']['media_brightcove_publisher_id']) ? $variables['options']['field']['widget']['media_brightcove_publisher_id'] : variable_get('media_brightcove_publisher_id', ''));
    $variables['publisher_id'] = check_plain($publisher_id);

    // Work out which player ID to use:
    $player_id = !empty($variables['player_id']) ? $variables['player_id'] : variable_get('media_brightcove_player_id', '');

    $variables['player_id'] = check_plain($player_id);
    $variables['video_id'] = check_plain($variables['video_id']);
    $variables['object_id'] = 'media-brightcove-video-'. ($count++);
    $variables['div_id'] = 'media-brightcove-'. ($count++);
    $variables['classes'] = array('media-brightcove');
    $variables['availability'] = TRUE;

    // Display a message if a video is currently unavailable.
    if (variable_get('media_brightcove_check_for_unavailable', FALSE) && ($library = libraries_detect('brightcove')) && !empty($library['installed']) && !media_brightcove_video_available($variables['video_id']) && ($unavailable_message = variable_get('media_brightcove_status_display_unavailable', t('This video is currently unavailable. Please check back later.')))) {
      $variables['availability'] = FALSE;
      $variables['output'] = theme('media_brightcove_unavailable_message', $unavailable_message);
    }
    else if (!empty($variables['publisher_id']) && !empty($variables['player_id'])) {
      // If SWFObject API is available, use it.
      if (function_exists('theme_swfobject_api')) {
        $url = 'http://c.brightcove.com/services/viewer/federated_f9/' . $variables['player_id'] . '?isVid=1&publisherID=' . $variables['publisher_id'];
        $params = array(
          'base' => 'http://admin.brightcove.com',
          'name' => 'flashObj',
          'width' => $variables['width'],
          'height' => $variables['height'],
          'seamlesstabbing' => 'false',
          'type' => 'application/x-shockwave-flash',
          'allowFullScreen' => 'true',
          'swLiveConnect' => 'true',
        );
        $vars = array(
          'videoId' => $variables['video_id'],
          'playerId' => $variables['player_id'],
        );
        $variables['output'] = theme('swfobject_api', array('url' => $url, 'params' => $params, 'vars' => $vars));
      }
      else {
        // @TODO: Tie this in.
        $autostart = !empty($variables['options']['autoplay']) ? 'autoStart=true' : 'autoStart=false';
        $variables['output'] = <<<OUTPUT
<object id="{$variables['object_id']}" width="{$variables['width']}" height="{$variables['height']}" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,47,0"><param name="movie" value="http://c.brightcove.com/services/viewer/federated_f9/{$variables['player_id']}?isVid=1&isUI=1&publisherID={$variables['publisher_id']}" /><param name="bgcolor" value="#FFFFFF" /><param name="flashVars" value="videoId={$variables['video_id']}&playerID={$variables['player_id']}&domain=embed&" /><param name="base" value="http://admin.brightcove.com" /><param name="seamlesstabbing" value="false" /><param name="allowFullScreen" value="true" /><param name="swLiveConnect" value="true" /><param name="allowScriptAccess" value="always" /><embed src="http://c.brightcove.com/services/viewer/federated_f9/{$variables['player_id']}?isVid=1&isUI=1&publisherID={$variables['publisher_id']}" bgcolor="#FFFFFF" flashVars="videoId={$variables['video_id']}&playerID={$variables['player_id']}&domain=embed&" base="http://admin.brightcove.com" name="flashObj" width="{$variables['width']}" height="{$variables['height']}" seamlesstabbing="false" type="application/x-shockwave-flash" allowFullScreen="true" swLiveConnect="true" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"></embed></object>
OUTPUT;
      }
    }
    else {
      $variables['output'] = '';
    }
  }
  else {
    $variables['availability'] = FALSE;
    if ($unavailable_message = variable_get('status_display_unavailable', t('This video is currently unavailable. Please check back later.'))) {
      $variables['output'] = theme('media_brightcove_unavailable_message', $unavailable_message);
    }
    else {
      $variables['output'] = '';
    }
  }
}
